import { Injectable } from '@angular/core';
import { Md5 } from 'ts-md5/dist/md5';
import { HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  baseUrl = "http://gateway.marvel.com/v1/public/characters?limit=100&"
  privateKey = "b6cf51687b9c3467eabe4184e3b063001b7cc5ee"
  publicKey= "c78f062873b73f3b7dc75e91bab0c84a"
  
  constructor(private http: HttpClient) { }

  marvel() {
    const timestamp = new Date().getTime()

    const hash = new Md5().appendStr(timestamp + this.privateKey + this.publicKey).end()

    return this.http.get(
      `${this.baseUrl}ts=${timestamp}&apikey=${this.publicKey}&hash=${hash}`
    )
  }
}
