import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  data: any[] = []

  constructor(private api: ApiService) {}

  ngOnInit() {
    this.api.marvel().subscribe((res: any) => {
      this.data = res.data.results
    })
  }

}
